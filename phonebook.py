from sys import exit


phonebook = dict()


def print_users():
    for key, value in phonebook.items():
        print(key, value)


def functions():
    print("1. Добавить абонента.")
    print("2. Удалить абонента.")
    print("3. Список абонентов.")
    print("4. Поиск по номеру.")
    print("5. Поиск по имени.")
    print("6. Выход")


def add_user():
    name = input("Введите имя: ")
    phone_number = input("Введите номер: ")
    phonebook[name] = phone_number
    print("Aбонент добавлен !")


def delete_user():
    remove = input("Введите имя абонента: ")
    if remove in phonebook.keys():
        del phonebook[remove]
        print("Абонент удален !")
    else:
        print("Абонент не найден.")


def search_by_name():
    try:
        name = input("Введите имя: ")
        print("Номер абонента {1} - {0}".format(phonebook[name], name))
    except KeyError:
        print("Абонент не найден.")


def search_by_number():
    number = input("Введите номер: ")
    if number in phonebook.values():
        for key in phonebook:
            if phonebook[key] == number:
                print("Номер принадлежит абоненту: {}.".format(key))
    else:
        print("Номер отсутствует в базе данных.")


def decisions():

    decision = input("Выберите опцию: ")

    if decision == "1":
        add_user()
        functions()
        decisions()
    elif decision == "2":
        delete_user()
        functions()
        decisions()
    elif decision == "3":
        print_users()
        functions()
        decisions()
    elif decision == "4":
        search_by_number()
        functions()
        decisions()
    elif decision == "5":
        search_by_name()
        functions()
        decisions()
    elif decision == "6":
        print("Завершение...")
        exit()
    else:
        print("Выберите опцию !")
        functions()
        decisions()


functions()
decisions()